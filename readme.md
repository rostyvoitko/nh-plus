# NHPlus

##Informationen zur Lernsituation
Du bist Mitarbeiter der HiTec GmbH, die seit über 15 Jahren IT-Dienstleister und seit einigen Jahren ISO/IEC 27001 zertifiziert ist. Die HiTec GmbH ist ein mittelgroßes IT-Systemhaus und ist auf dem IT-Markt mit folgenden Dienstleistungen und Produkten vetreten: 

Entwicklung: Erstellung eigener Softwareprodukte

Consulting: Anwenderberatung und Schulungen zu neuen IT- und Kommunikationstechnologien , Applikationen und IT-Sicherheit

IT-Systembereich: Lieferung und Verkauf einzelner IT-Komponenten bis zur Planung und Installation komplexer Netzwerke und Dienste

Support und Wartung: Betreuung von einfachen und vernetzten IT-Systemen (Hard- und Software)

Für jede Dienstleistung gibt es Abteilungen mit spezialisierten Mitarbeitern. Jede Abteilung hat einen Abteilungs- bzw. Projektleiter, der wiederum eng mit den anderen Abteilungsleitern zusammenarbeitet.

 

##Projektumfeld und Projektdefinition

Du arbeitest als Softwareentwickler in der Entwicklungsabteilung. Aktuell bist du dem Team zugeordnet, das das Projekt "NHPlus" betreut. Dessen Auftraggeber - das Betreuungs- und Pflegeheim "Curanum Schwachhausen" - ist ein Pflegeheim im Bremer Stadteil Schwachhausen - bietet seinen in eigenen Zimmern untergebrachten Bewohnern umfangreiche Therapie- und Serviceleistungen an, damit diese so lange wie möglich selbstbestimmt und unabhängig im Pflegeheim wohnen können. Curanum Schwachhausen hat bei der HiTec GmbH eine Individualsoftware zur Verwaltung der Patienten und den an ihnen durchgeführten Behandlungen in Auftrag gegeben. Aktuell werden die Behandlungen direkt nach ihrer Durchführung durch die entsprechende Pflegekraft handschriftlich auf einem Vordruck erfasst und in einem Monatsordner abgelegt. Diese Vorgehensweise führt dazu, dass Auswertungen wie z.B. welche Behandlungen ein Patient erhalten oder welche Pflegkraft eine bestimmte Behandlung durchgeführt hat, einen hohen Arbeitsaufwand nach sich ziehen. Durch NHPlus soll die Verwaltung der Patienten und ihrer Behandlungen elektronisch abgebildet und auf diese Weise vereinfacht werden.

Bei den bisher stattgefundenen Meetings mit dem Kunden konnten folgende Anforderungen an NHPlus identifiziert werden:

- alle Patienten sollen mit ihrem vollen Namen, Geburtstag, Pflegegrad, dem Raum, in dem sie im Heim untergebracht sind, sowie ihrem Vermögensstand erfasst werden.

- Die Pflegekräfte werden mit ihrem vollen Namen und ihrer Telefonnummer erfasst, um sie auf Station schnell erreichen zu können.

- jede Pflegekraft erfasst eine Behandlung elektronisch, indem sie den Patienten, das Datum, den Beginn, das Ende, die Behandlungsart sowie einen längeren Text zur Behandlung erfasst.

- Die Software muss den Anforderungen des Datenschutzes entsprechen. 

- Die Software ist zunächst als Desktopanwendung zu entwickeln, da die Pflegekräfte ihre Behandlungen an einem stationären Rechner in ihrem Aufenthaltsraum erfassen sollen.

 

Da in der Entwicklungsabteilung der HiTech GmbH agile Vorgehensweisen vorgeschrieben sind, wurde für NHPlus Scum als Vorgehensweise gewählt.

 

##Stand des Projektes

In den bisherigen Sprints wurden die Module zur Erfassung der Patienten- und Behandlungsdaten fertiggestellt. Es fehlt das Modul zur Erfassung der Pflegekräfte. Deswegen kann bisher ebenfalls nicht erfasst werden, welche Pflegekraft eine bestimmte Behandlung durchgeführt hat. In der letzten Sprint Review sind von der Curanum Schwachhausen Zweifel angebracht worden, dass die bisher entwickelte Software den Anforderungen des Datenschutzes genügt.

## Technische Hinweise

Wird das Open JDK verwendet, werden JavaFX-Abhängigkeiten nicht importiert. Die Lösung besteht in der Installation der neuesten JDK-Version der Firma Oracle.

## Technische Hinweise zur Datenbank

- Benutzername: SA
- Passwort: SA
- Bitte nicht in die Datenbank schauen, während die Applikation läuft. Das sorgt leider für einen Lock, der erst wieder verschwindet, wenn IntelliJ neugestartet wird!

## Benutzername und Passwort für das Login
- Benutzername Login: admin
- Passwort Login: 123

## Tests

TASK 1
1. Spalte Vermögensstand ist aus der Patientenliste verschwunden.<br>
    Beim Öffnen der Patientenliste ist die Spalte Vermögensstand nicht mehr zu sehen.
2. Textfeld Vermögensstand ist aus der Eingabeleiste verschwunden.<br>
    Auch in der Eingabeleiste ist das Textfeld Vermögensstand verschwunden.
3. Spalte Vermögensstand aus der Datenbank verschwunden.<br>
    Beim Öffnen der Datenbank ist die Datenbankspalte entfernt.

TASK 2
1. Beim Start der Anwendung erscheint zunächst nur die Login Maske.<br>
    Das Programmstart zeigt sich zunächst das Login-fenster.
2. Passwort liegt verschlüsselt in der Datenbank vor.<br>
    Das Passwort liegt in Hashcodeform vor.
3. Programm wird nur gestartet wenn der Nutzer in der Datenbank vorhanden ist.<br>
    Eingabe eines falschen Passwort löst eine Fehlermeldung aus. Sollte der Eingabebutton betätigt werden, ohne dass Daten eingegeben worden sind, erscheint auch hier eine zugehörige Fehlermeldung. Nur wenn das passende passwort eingegeben wird, erscheint zunächst die Meldung über bereinigte Daten und danach das Startfenster.
4. Abbrechen Button schließt die Maske.<br>
    Funktioniert, wie vorgesehen. Diese Funktion ist vielleicht etwas redundant, da es schon einen Standard-Schließbutton in der Kopfzeile gibt. Vielleicht könnte mann so gänzlich auf eine Kopfzeile im Login Fenster verzichten?
5. O.K. Button leitet den Verifizierungsprozess der eingegebenen Daten ein.<br>
    Wie schon bei Punkt 3 erwähnt, funktioniert der Verifizierungsprozeß einwandfrei.

TASK 3
1. Stellen sie sicher, dass neue Patienten korrekte Eintrittsdaten als Zeitstempel haben.<br>
    Der Zeitstempel ist aktuell.
2. Stellen Sie sicher dass es keine Datensätze gibt die älter als 10 Jahre sind.<br>
    Kein Datensatz ist älter als 10 Jahre.
3. Taste Behandlungsabbruch testen.<br>
    Nach der Betätigung des Behandlungsabbruch-Button verschwindet der ausgewählte Datensatz aus der Übersicht und wird für die nächste 10 Jahre archiviert.
TASK 4
1. Pfleger/innen Button existiert.<br>
    In der Startmaske existiert der Button.
2. Pfleger/innen Button funktioniert.<br>
    Bei Betätigung des Buttons öffnet sich die Maske mit der Übersicht über die Pfleger/Innen.
3. Pfleger/innen Übersicht verfügt über die Spalten PflegerID, Vorname, Nachname und Telefonnummer.<br>
    Das ist der Fall.

TASK 5
1. Übersicht Behandlungen ist um die Spalte Pfleger/in erweitert.<br>
    Dem ist so.
2. In der Spalte Pfleger/in ist der/die Pfleger/in mit vollem Namen und Telefonnummer aufgeführt.<br>
    Auch das funktioniert.
3. In der Eingabe Pfleger/in läßt sich Pfleger/innen eingeben.<br>
    Pflegekraft läßt sich eingeben und löschen (archivieren). 

TASK 6
1. Bei Neustart sollten die Daten weiterhin vorhanden sein.<br>
    Eingegebene Daten sind nach Neustart weiterhin vorhanden.

