package mock;

import datastorage.*;
import model.Nurse;
import model.Patient;
import model.Treatment;
import model.User;
import utils.DateConverter;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;


/**
 * Class do add NHPlus data to database for testing purposes.
 */
public class TestRecordsAdder {

    private final PatientDAO patientDAO;
    private final NurseDAO nurseDAO;
    private final TreatmentDAO treatmentDAO;
    private final UserDAO userDAO;

    public TestRecordsAdder() {
        DAOFactory factory = DAOFactory.getDAOFactory();
        this.nurseDAO = factory.createNurseDAO();
        this.patientDAO = factory.createPatientDAO();
        this.treatmentDAO = factory.createTreatmentDAO();
        this.userDAO = factory.createUserDAO();
    }

    /**
     * adds a treatment that was made 10 years ago.
     * Due to DSGVO policies and implementation of {@link datastorage.DatabaseCleaner},
     * this treatment must be deleted when the application start
     * @throws SQLException - when something went wrong while executing sql queries
     */
    public void add10YearsOldTreatment() throws SQLException {
        LocalDate date = LocalDate.now();
        date = date.minusYears(10).minusDays(1);
        LocalTime begin = DateConverter.convertStringToLocalTime("10:00");
        LocalTime end = DateConverter.convertStringToLocalTime("11:00");
        Treatment treatment = new Treatment(19, date, begin, end, "Testing deleting treatment with date " + date, "Test");
        treatment.setNurseId("");
        treatmentDAO.create(treatment);
    }

    /**
     * adds a new patient.
     * This patient is archived and exited 10 years ago.
     * He should be deleted when the app starts,
     * due to DSGVO policies and implementation of {@link datastorage.DatabaseCleaner}.
     * @throws SQLException - when something went wrong while executing sql queries
     */
    public void addArchived10YearsOldPatient() throws SQLException {
        LocalDate enterDate = LocalDate.now().minusYears(10).minusDays(1);
        LocalDate birthDate = LocalDate.now().minusYears(65);
        Patient p =  new Patient("Testing deleting archived", "exited 10 years ago", enterDate, birthDate, "5", "303");
        p.setArchive(true);
        p.setExitDate(enterDate.plusDays(1));
        patientDAO.create(p);
    }

    /**
     * adds a patient that was created 10 years ago and has no treatments.
     * Due to DSGVO policies and implementation of {@link datastorage.DatabaseCleaner},
     * this patient should be deleted during app start.
     * @throws SQLException - when something went wrong while executing sql queries
     */
    public void add10YearsAgoCreatedPatient() throws SQLException {
        LocalDate enterDate = LocalDate.now().minusYears(10).minusDays(1);
        LocalDate birthDate = LocalDate.now().minusYears(65);
        Patient p =  new Patient("10 years ago", "created and not archived", enterDate, birthDate, "5", "303");
        patientDAO.create(p);
    }

    /**
     * adds a nurse. This nurse is archived and exited 10 years ago.
     * Due to DSGVO this nurse should be deleted during app's start
     * @throws SQLException - when something went wrong while executing sql queries
     */
    public void add10YearsAgoExitedArchivedNurse() throws SQLException {
        LocalDate exitDate = LocalDate.now().minusYears(10).minusDays(1);
        LocalDate enterDate = LocalDate.now().minusYears(30);
        Nurse nurse =  new Nurse("10 years ago exited, archived, ", "created Nurse", enterDate, exitDate, "435435325");
        nurse.setArchive(true);
        nurseDAO.create(nurse);
    }

    /**
     * adds a user to NHPLus application if the user not exists
     * @throws SQLException - if something wrong while interaction with DB happens
     */
    public void addAppUser() throws SQLException {
        User existingUser = userDAO.getUserByUsername("admin");
        if(existingUser != null) {
            return;
        }
        String password = "123";
        String username = "admin";
        User user = new User(username, password);
        userDAO.create(user);
    }
}
