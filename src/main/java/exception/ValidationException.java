package exception;

import java.util.List;

/**
 * Exception for validation errors
 */
public class ValidationException extends Exception{

    private List<String> errors;

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(List<String> errors) {
       this.errors = errors;
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public List<String> getErrors() {
        return errors;
    }
}
