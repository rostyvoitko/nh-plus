package validation;

import exception.ValidationException;

import java.util.ArrayList;
import java.util.List;

public class InputValidator {

    public static void validateAddingNurseInput(String firstName, String surname, String number) throws ValidationException {
        List<String> errors = new ArrayList<>();
        if(!validateGermanPhoneNumber(number)) {
            errors.add("Die Nummer soll in +49 XXX XXXXXXX Format sein");
        }
        if(firstName.equals("")) {
            errors.add("Der Name darf nicht leer sein");
        }
        if(surname.equals("")) {
            errors.add("Der Nachname darf nicht leer sein");
        }
        if(!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }

    public static boolean validateGermanPhoneNumber(String number){
        String regex = "^\\+49\\s\\d{3}\\s\\d{6,9}$";
        return number.matches(regex);
    }
}
