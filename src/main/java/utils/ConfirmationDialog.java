package utils;

import controller.ConfirmationDialogController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Class ConfirmationDialog
 * contains a static method to get a confirmation dialog for some action in the application
 */
public class ConfirmationDialog {

    /**
     * returns a confirmation window to delete an item
     * @param itemName - string - item name to delete
     * @param fxmlLoader - fxml loader with loaded view
     * @param confirmationDialog - parent
     * @return - confirmation dialog controller waiting for response
     */
    public static ConfirmationDialogController getConfirmationDialogController(String itemName, FXMLLoader fxmlLoader, Parent confirmationDialog) {
        ConfirmationDialogController confirmationDialogController = fxmlLoader.getController();
        confirmationDialogController.setMessage("Sind Sie sicher, dass Sie diese Eintrag löschen möchten? Eintrag: " + itemName + "?");
        Stage confirmationDialogStage = new Stage();
        confirmationDialogStage.initModality(Modality.APPLICATION_MODAL);
        confirmationDialogStage.setScene(new Scene(confirmationDialog));
        confirmationDialogStage.showAndWait();
        return confirmationDialogController;
    }
}
