package utils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Utility class for converting Strings to LocalDate and LocalTime and vice versa
 */
public class DateConverter {

    /**
     * Constructor for {@link DateConverter}
     */
    private DateConverter() {
        // no args constructor
    }

    /**
     * converts string in format of {@link DateConverter#getFormatter()} to LocalDate
     * @param date - String in format from {@link DateConverter#getFormatter()}
     * @return - LocalDate
     */
    public static LocalDate convertStringToLocalDate(String date) {
        if(date == null) {
            return null;
        }
        String[] array = date.split("-");
        return LocalDate.of(Integer.parseInt(array[0]), Integer.parseInt(array[1]),
                Integer.parseInt(array[2]));
    }

    /**
     * converts String in form "hh:mm" to LocalTime Instance
     * @param time - String in format hh:mm
     * @return - localtime instance
     */
    public static LocalTime convertStringToLocalTime(String time) {
        String[] array = time.split(":");
        return LocalTime.of(Integer.parseInt(array[0]), Integer.parseInt(array[1]));
    }

    /**
     * formatter for NHPlus app used to convert strings in format yyyy-MM-dd to java
     * LocalDate and LocalTime instances
     * @return - {@link DateTimeFormatter} in format yyyy-MM-dd
     */
    public static DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }
}
