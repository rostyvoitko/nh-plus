package utils;

import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.util.StringConverter;
import model.Nurse;

/**
 * Class with static methods to configure the comboboxes in app
 */
public class ComboBoxConfigurer {

    private ComboBoxConfigurer() {
    }

    public static void configureNurseComboBox(ComboBox<Nurse> nurseComboBox) {
        nurseComboBox.setCellFactory(cell -> new ListCell<>() {
            @Override
            protected void updateItem(Nurse nurse, boolean empty) {
                super.updateItem(nurse, empty);
                if (empty || nurse == null) {
                    setText(null);
                } else {
                    setText(nurse.getFirstName() + " " + nurse.getSurname());
                }
            }
        });

        nurseComboBox.setConverter(new StringConverter<>() {
            @Override
            public String toString(Nurse nurse) {
                if (nurse == null) {
                    return null;
                } else {
                    return nurse.getFirstName() + " " + nurse.getSurname();
                }
            }

            @Override
            public Nurse fromString(String string) {
                return null;
            }
        });
    }
}
