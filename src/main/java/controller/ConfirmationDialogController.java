package controller;


import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * Controller to show a confirmation before archiving or deleting data
 */
public class ConfirmationDialogController {
    @FXML private Label messageLabel;
    private boolean confirmed;

    /**
     * sets the confirmation message
     * @param message - String - message
     */
    public void setMessage(String message) {
        messageLabel.setWrapText(true);
        messageLabel.setText(message);
    }

    /**
     *
     * @return true if was confirmed by user, otherwise false
     */
    public boolean isConfirmed() {
        return confirmed;
    }

    @FXML
    private void handleYes() {
        confirmed = true;
        closeWindow();
    }

    @FXML
    private void handleNo() {
        confirmed = false;
        closeWindow();
    }

    private void closeWindow() {
        ((Stage)messageLabel.getScene().getWindow()).close();
    }
}