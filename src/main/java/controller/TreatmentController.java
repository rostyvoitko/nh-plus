package controller;

import datastorage.DAOFactory;
import datastorage.NurseDAO;
import datastorage.PatientDAO;
import datastorage.TreatmentDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Nurse;
import model.Patient;
import model.Treatment;
import utils.ComboBoxConfigurer;
import utils.DateConverter;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Controller for the view where user can edit the treatment.
 */
public class TreatmentController {
    @FXML
    private Label lblPatientName;
    @FXML
    private Label lblCarelevel;
    @FXML
    private TextField txtBegin;
    @FXML
    private TextField txtEnd;
    @FXML
    private TextField txtDescription;
    @FXML
    private TextArea taRemarks;
    @FXML
    private DatePicker datepicker;
    @FXML
    private ComboBox<Nurse> nurseComboBox;

    private AllTreatmentController controller;
    private Stage stage;
    private Patient patient;
    private Treatment treatment;

    /**
     * initializes the controller
     * @param controller - {@link AllTreatmentController}
     * @param stage - {@link Stage} stage
     * @param treatment - {@link Treatment} - treatment to be edited
     */
    public void initializeController(AllTreatmentController controller, Stage stage, Treatment treatment) {
        this.stage = stage;
        this.controller= controller;
        PatientDAO pDao = DAOFactory.getDAOFactory().createPatientDAO();
        try {
            configureComboBox(treatment.getNurseId());
            this.patient = pDao.read((int) treatment.getPid());
            this.treatment = treatment;
            showData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * show the data of treatment on the view
     */
    private void showData(){
        this.lblPatientName.setText(patient.getSurname()+", "+patient.getFirstName());
        this.lblCarelevel.setText(patient.getCareLevel());
        LocalDate date = DateConverter.convertStringToLocalDate(treatment.getDate());
        this.datepicker.setValue(date);
        this.txtBegin.setText(this.treatment.getBegin());
        this.txtEnd.setText(this.treatment.getEnd());
        this.txtDescription.setText(this.treatment.getDescription());
        this.taRemarks.setText(this.treatment.getRemarks());
    }

    /**
     * handle change button
     */
    @FXML
    public void handleChange(){
        this.treatment.setDate(this.datepicker.getValue().toString());
        this.treatment.setBegin(txtBegin.getText());
        this.treatment.setEnd(txtEnd.getText());
        this.treatment.setDescription(txtDescription.getText());
        this.treatment.setRemarks(taRemarks.getText());
        this.treatment.setNurseId(String.valueOf(nurseComboBox.getSelectionModel().getSelectedItem().getNurseID()));
        doUpdate();
        controller.readAllAndShowInTableView();
        stage.close();
    }

    /**
     * do update of treatment the in database
     */
    private void doUpdate(){
        TreatmentDAO dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        try {
            dao.update(treatment);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * handle cancel button
     */
    @FXML
    public void handleCancel(){
        stage.close();
    }

    /**
     * configure combo box for selecting a nurse.
     * selectedNurseID is empty string ? combobox selection will be empty
     * selectedNurseID not empty ? the nurse with such an id will be selected
     * @param selectedNurseID - treatment's nurseID
     * @throws SQLException - when something went wrong while database interaction
     */
    private void configureComboBox(String selectedNurseID) throws SQLException {
        NurseDAO nurseDAO = DAOFactory.getDAOFactory().createNurseDAO();
        ObservableList<Nurse> nurseObservableList = FXCollections.observableArrayList(nurseDAO.readAll());
        nurseComboBox.setItems(nurseObservableList);
        if(selectedNurseID != null && !selectedNurseID.isEmpty()) {
            Optional<Nurse> selectedNurse = nurseObservableList.stream()
                    .filter(n -> String.valueOf(n.getNurseID()).equals(selectedNurseID)).findFirst();
            selectedNurse.ifPresent(nurse -> nurseComboBox.getSelectionModel().select(nurse));
        }
        ComboBoxConfigurer.configureNurseComboBox(nurseComboBox);
    }
}