package controller;

import datastorage.DAOFactory;
import datastorage.NurseDAO;
import exception.ValidationException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Nurse;
import utils.ConfirmationDialog;
import validation.InputValidator;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The <code>AllNurseController</code> contains the entire logic of the nurses view.
 * It determines which data is displayed and how to react to events.
 */
public class AllNursesController {
    private ObservableList<Nurse> tableviewContent = FXCollections.observableArrayList();
    private NurseDAO dao;
    @FXML
    Button btnDelete;
    @FXML
    Button btnAdd;
    @FXML
    private TableColumn<Nurse, String> colSurname;
    @FXML
    private TableColumn<Nurse, Integer> colID;
    @FXML
    private TableColumn<Nurse, String> colFirstName;
    @FXML
    private TableColumn<Nurse, String> colPhoneNumber;
    @FXML
    private TableView<Nurse> tableView;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField phoneNumberField;

    /**
     * method that be called while initializing a {@link AllNursesController}
     * init data to show,
     * configure table.
     */
    public void initialize() {
       readAllAndShowInTableView();

        this.colID.setCellValueFactory(new PropertyValueFactory<Nurse, Integer>("nurseID"));

        this.colFirstName.setCellValueFactory(new PropertyValueFactory<Nurse, String>("firstName"));
        this.colFirstName.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colSurname.setCellValueFactory(new PropertyValueFactory<Nurse, String>("surname"));
        this.colSurname.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colPhoneNumber.setCellValueFactory(new PropertyValueFactory<Nurse, String>("phoneNumber"));
        this.colPhoneNumber.setCellFactory(TextFieldTableCell.forTableColumn());

        this.tableView.setItems(tableviewContent);
    }


    /**
     * Calls readAll in {@link NurseDAO} and shows patients in the table
     */
    private void readAllAndShowInTableView() {
        this.tableviewContent.clear();
        this.dao = DAOFactory.getDAOFactory().createNurseDAO();
        List<Nurse> allNurses;
        try {
            allNurses = dao.readAll().stream().filter(n -> !n.getArchive()).collect(Collectors.toList());
            this.tableviewContent.addAll(allNurses);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * handles new surname value
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditSurname(TableColumn.CellEditEvent<Nurse, String> event){
        event.getRowValue().setSurname(event.getNewValue());
        doUpdate(event);
    }

    /**
     * updates a patient by calling the update-Method in the {@link NurseDAO}
     * @param t row to be updated by the user (includes the patient)
     */
    private void doUpdate(TableColumn.CellEditEvent<Nurse, String> t) {
        try {
            dao.update(t.getRowValue());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * handles a add-click-event.
     * Creates a patient and calls the create method in the {@link NurseDAO}
     * Validates data vefore adding with {@link InputValidator}
     */
    @FXML
    public void handleAdd() {
        String surname = this.lastNameField.getText();
        String firstname = this.firstNameField.getText();
        String phoneNumber = this.phoneNumberField.getText();
        try {
            InputValidator.validateAddingNurseInput(firstname, surname, phoneNumber);
        } catch (ValidationException ex) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Validation Probleme...");
            alert.setHeaderText(null);
            alert.setContentText(String.join("; \n", ex.getErrors()));
            alert.showAndWait();
            return;
        }
        try {
            Nurse n = new Nurse(firstname, surname, null, phoneNumber);
            dao.create(n);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        readAllAndShowInTableView();
        clearTextfields();
    }

    /**
     * handles a delete-click-event.
     * Calls the archive method
     */
    @FXML
    public void handleDeleteRow() {
        Nurse selectedItem = this.tableView.getSelectionModel().getSelectedItem();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ConfirmationDialog.fxml"));
            Parent confirmationDialog = fxmlLoader.load();
            ConfirmationDialogController confirmationDialogController = ConfirmationDialog.getConfirmationDialogController(
                    selectedItem.getSurname(), fxmlLoader, confirmationDialog);
            if(confirmationDialogController.isConfirmed()) {
                archiveNurse(selectedItem);
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Marks the selected nurse as archived in databases and removes from table in UI
     * @param selectedItem - Nurse - selected Nurse to archive
     * @throws SQLException - error if something happened while db query
     */
    private void archiveNurse(Nurse selectedItem) throws SQLException {
        dao.markAsArchived(selectedItem);
        this.tableView.getItems().remove(selectedItem);
    }

    /**
     * removes content from all textfields
     */
    private void clearTextfields() {
        this.phoneNumberField.clear();
        this.firstNameField.clear();
        this.lastNameField.clear();
    }
}
