package controller;

import constants.NHPlusConstants;
import datastorage.DatabaseCleaner;
import datastorage.DatabaseCleanerStreamsImpl;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import mock.TestRecordsAdder;
import model.Nurse;
import model.Patient;
import model.Treatment;

import java.sql.SQLException;
import java.util.List;

/**
 * Controller for DatabaseCleanView
 */
public class DatabaseCleanController {

    @FXML
    private Button continueButton;
    @FXML
    private Label label;
    @FXML
    private ProgressBar progress;
    private final Alert errorAlert;
    private final Alert successAlert;
    private final DatabaseCleaner databaseCleaner;
    private final TestRecordsAdder testRecords;

    /**
     * constructor for {@link DatabaseCleanController}
     * creates instances of needed dependencies
     */
    public DatabaseCleanController() {
        this.databaseCleaner = new DatabaseCleanerStreamsImpl();
        this.testRecords = new TestRecordsAdder();
        this.errorAlert = new Alert(Alert.AlertType.ERROR);
        this.successAlert = new Alert(Alert.AlertType.INFORMATION);
    }

    /**
     * method that is called when {@link DatabaseCleanController} is initialized
     * calls methods in {@link TestRecordsAdder} to add tests data
     * @throws SQLException - if something went wrong while db interaction
     */
    public void initialize() throws SQLException {
        // add mocked test records
        testRecords.add10YearsOldTreatment();
        testRecords.add10YearsAgoCreatedPatient();
        testRecords.addArchived10YearsOldPatient();
        testRecords.add10YearsAgoExitedArchivedNurse();

        continueButton.setDisable(true);
        continueButton.setOnAction(e -> continueButtonClicked());
        Task<Void> task = getCleaningTask();
        configureTask(task);

        // set progress bar property
        progress.progressProperty().bind(task.progressProperty());

        // start thread with task
        Thread thread = new Thread(task);
        thread.start();
    }

    /**
     * Creates a task to clean the database
     * @return - {@link Task<Void>} cleaning task
     */
    public Task<Void> getCleaningTask () {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                List<Patient> deletedPatient = databaseCleaner.deleteOldArchivedPatientsData();
                updateProgress(25, 100);
                List<Nurse> deletedNurses = databaseCleaner.deleteArchivedNurses();
                updateProgress(50, 100);
                deletedPatient.addAll(databaseCleaner.deleteOldPatients());
                updateProgress(75, 100);
                List<Treatment> treatments = databaseCleaner.deleteOldTreatments();
                updateProgress(100, 100);
                updateMessage(NHPlusConstants.CLEANED_MESSAGE);
                updateUI(deletedNurses, deletedPatient, treatments);
                continueButton.setDisable(false);
                return null;
            }
        };
    }

    /**
     * updates up
     * @param nurses - the list of deleted nurses
     * @param patients - the list of deleted patients
     * @param treatments - the list of deleted treatments
     */
    public void updateUI(List<Nurse> nurses, List<Patient> patients, List<Treatment> treatments) {
        StringBuilder sb = new StringBuilder();
        sb.append("Folgende Patienten wurden gelöscht:\n");
        for (Patient patient : patients) {
            sb.append(patient.getFirstName()).append(" ").append(patient.getSurname()).append("\n");
        }
        sb.append("\nFolgende Krankenschwestern wurden gelöscht:\n");
        for (Nurse nurse : nurses) {
            sb.append(nurse.getFirstName()).append(" ").append(nurse.getSurname()).append("\n");
        }
        sb.append("\nFolgende Behandlungen wurden gelöscht:\n");
        for (Treatment treatment : treatments) {
            sb.append(treatment.getDescription()).append("\n");
        }
        successAlert.setHeaderText(NHPlusConstants.CLEANED_MESSAGE);
        successAlert.setContentText(sb.toString());
        continueButton.setDisable(false);
    }

    public void continueButtonClicked() {
        Stage stage = (Stage) continueButton.getScene().getWindow();
        stage.close();
    }

    /**
     * configures task from {@link DatabaseCleanController#getCleaningTask()}
     * @param task - {@link Task<Void>}
     */
    private void configureTask(Task<Void> task) {
        task.setOnFailed(e -> {
            Throwable ex = task.getException();
            ex.printStackTrace();
            errorAlert.setTitle("Fehler");
            errorAlert.setHeaderText(NHPlusConstants.ERROR_DB_MESSAGE);
            errorAlert.setContentText(ex.getMessage());
            errorAlert.showAndWait();
            Stage stage = (Stage) continueButton.getScene().getWindow();
            stage.close();
        });
        task.setOnSucceeded(e -> {
            label.setText(NHPlusConstants.CLEANED_MESSAGE);
            successAlert.showAndWait();
            Stage stage = (Stage) continueButton.getScene().getWindow();
            stage.close();
        });
    }
}
