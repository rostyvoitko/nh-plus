package controller;

import auth.AuthenticationContext;
import datastorage.ConnectionBuilder;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

/**
 * Main class if NHPlus application.
 * Extends {@link Application}
 */
public class Main extends Application {

    private Stage primaryStage;
    private Stage loginStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.loginStage = new Stage();
        showLoginWindow();
        if(AuthenticationContext.isAuthorized()) {
            mainWindow();
        }
    }

    /**
     * logic to control main window flow
     */
    public void mainWindow() {
        try {
            showDatabaseCleanView();
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/MainWindowView.fxml"));
            BorderPane pane = loader.load();

            Scene scene = new Scene(pane);
            this.primaryStage.setTitle("NHPlus");
            this.primaryStage.setScene(scene);
            this.primaryStage.setResizable(true);
            this.primaryStage.show();

            this.primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent e) {
                    ConnectionBuilder.closeConnection();
                    Platform.exit();
                    System.exit(0);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * loads DatabaseCleanView and creates a nes Stage for that
     */
    public void showDatabaseCleanView() {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/DatabaseCleanView.fxml"));
            AnchorPane pane = loader.load();
            Stage databaseCleanStage = new Stage();
            databaseCleanStage.setTitle("Database Clean");
            databaseCleanStage.setScene(new Scene(pane));
            databaseCleanStage.setResizable(false);
            databaseCleanStage.initModality(Modality.APPLICATION_MODAL);
            databaseCleanStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * loads LoginWindowView and shows its stage
     */
    public void showLoginWindow() {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/LoginWindowView.fxml"));
            BorderPane pane = loader.load();
            Scene loginScene = new Scene(pane);
            loginStage.setScene(loginScene);
            loginStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}