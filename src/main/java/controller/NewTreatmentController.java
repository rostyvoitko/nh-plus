package controller;

import datastorage.DAOFactory;
import datastorage.NurseDAO;
import datastorage.TreatmentDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.Nurse;
import model.Patient;
import model.Treatment;
import utils.ComboBoxConfigurer;
import utils.DateConverter;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Controller for NewTreatmentView to add a new treatment
 */
public class NewTreatmentController {
    @FXML
    private Label lblSurname;
    @FXML
    private Label lblFirstname;
    @FXML
    private TextField txtBegin;
    @FXML
    private TextField txtEnd;
    @FXML
    private TextField txtDescription;
    @FXML
    private TextArea taRemarks;
    @FXML
    private DatePicker datepicker;
    @FXML
    private ComboBox<Nurse> nurseComboBox;
    private final ObservableList<Nurse> nurseComboBoxData = FXCollections.observableArrayList();
    private AllTreatmentController controller;
    private Patient patient;
    private Stage stage;

    /**
     * method is called when the {@link NewTreatmentController} is initialized bi JavaFX
     * calls methods to configure combobox and create its data,
     * calls method to show patients data
     * @param controller - {@link AllTreatmentController}
     * @param stage - {@link Stage}
     * @param patient - {@link Patient}
     */
    public void initialize(AllTreatmentController controller, Stage stage, Patient patient) {
        configureComboBox();
        createComboBoxData();
        this.nurseComboBox.setItems(nurseComboBoxData);
        this.controller= controller;
        this.patient = patient;
        this.stage = stage;
        showPatientData();
    }

    private void showPatientData(){
        this.lblFirstname.setText(patient.getFirstName());
        this.lblSurname.setText(patient.getSurname());
    }

    /**
     * handles add button
     */
    @FXML
    public void handleAdd(){
        LocalDate date = this.datepicker.getValue();
        LocalTime begin = DateConverter.convertStringToLocalTime(txtBegin.getText());
        LocalTime end = DateConverter.convertStringToLocalTime(txtEnd.getText());
        String description = txtDescription.getText();
        Nurse selectedNurse = nurseComboBox.getSelectionModel().getSelectedItem();
        String nurseId = selectedNurse == null ? "" : String.valueOf(selectedNurse.getNurseID());
        String remarks = taRemarks.getText();
        Treatment treatment = new Treatment(patient.getPid(), date,
                begin, end, description, remarks);
        treatment.setNurseId(nurseId);
        createTreatment(treatment);
        controller.readAllAndShowInTableView();
        stage.close();
    }

    /**
     * handles cancel button
     */
    @FXML
    public void handleCancel(){
        stage.close();
    }

    private void createComboBoxData() {
        NurseDAO nurseDAO = DAOFactory.getDAOFactory().createNurseDAO();
        try {
            List<Nurse> nursesList = nurseDAO.readAll();
            this.nurseComboBoxData.addAll(nursesList);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void configureComboBox() {
        ComboBoxConfigurer.configureNurseComboBox(nurseComboBox);
    }

    private void createTreatment(Treatment treatment) {
        TreatmentDAO dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        try {
            dao.create(treatment);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}