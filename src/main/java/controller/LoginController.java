package controller;

import auth.AuthenticationContext;
import datastorage.DAOFactory;
import datastorage.UserDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mock.TestRecordsAdder;
import model.User;
import utils.PasswordHasher;

import java.sql.SQLException;
import java.util.Objects;

/**
 * Controller for the Login View to control user auth
 */
public class LoginController {

    @FXML
    private Button cancelButton;
    @FXML
    private Button okButton;
    @FXML
    private TextField userNameTextfield;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label messageLabel;

    /**
     * constructor for LoginController
     */
    public LoginController() {
        final TestRecordsAdder mockRecords = new TestRecordsAdder();
        try {
            mockRecords.addAppUser();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * initialization method for JavaFX controller
     */
    public void initialize() {
        // nothing to initialize
    }

    /**
     * handler for a cancel button
     */
    public void handleCancelButton() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    /**
     * handler for OK Button
     * forwards to Main-window if Login-data is correct
     */
    public void handleOkButton() {
        if (!userNameTextfield.getText().isBlank() && !passwordField.getText().isBlank()) {
            messageLabel.setText("Login");
            if (checkAuthentification(userNameTextfield.getText(), passwordField.getText())) {
                AuthenticationContext.authorize();
                Stage stage = (Stage) okButton.getScene().getWindow();
                stage.close();
            } else {
                messageLabel.setWrapText(true);
                messageLabel.setText("Benutzer und/oder Passwort unbekannt");
            }
        } else {
            messageLabel.setText("Bitte Benutzername und Passwort eingeben");
        }
    }

    /**
     * takes input and counts matching pairs in database
     * @param user input from Login Text field
     * @param password input from password field
     * @return true if there ist one matching pair in database
     */
    private boolean checkAuthentification(String user, String password) {
        String hashedPassword = PasswordHasher.doHashing(password);
        UserDAO userDAO = DAOFactory.getDAOFactory().createUserDAO();
        try {
            User foundUser = userDAO.getUserByUsername(user);
            return foundUser != null && Objects.equals(foundUser.getPassword(), hashedPassword);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
