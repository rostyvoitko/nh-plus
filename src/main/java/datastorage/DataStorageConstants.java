package datastorage;

/**
 * Constants for datastorage package
 */
public class DataStorageConstants {

    private DataStorageConstants() {
    }

    public static final int YEARS_TO_STORE_DATA = 10;
}
