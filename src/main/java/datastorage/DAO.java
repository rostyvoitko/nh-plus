package datastorage;

import java.sql.SQLException;
import java.util.List;

/**
 * DAO interface to provide CRUD for a given type
 * @param <T> - type of model
 */
public interface DAO<T> {

    /**
     * creates a given t instance in db
     * @param t - object to create
     * @throws SQLException - when something went wring while interacting with database
     */
    void create(T t) throws SQLException;

    /**
     * read an object from db with a given key
     * @param key - primary key of object to be found
     * @return - found object
     * @throws SQLException - when something went wrong while interacting with database
     */
    T read(long key) throws SQLException;

    /**
     * read all objects from database
     * @return - returns all objects of given class type from database
     * @throws SQLException - when something went wrong while interacting with database
     */
    List<T> readAll() throws SQLException;

    /**
     * update an object in database
     * @param t - object to update
     * @throws SQLException - when something went wrong while interacting with database
     */
    void update(T t) throws SQLException;

    /**
     * delete object from db by a given key
     * @param key - primary key of object to be deleted
     * @throws SQLException - when something went wrong while interacting with database
     */
    void deleteById(long key) throws SQLException;

    /**
     * delete all objects from db whose ids are in the given ids list
     * @param ids - List of ds
     * @throws SQLException - when something went wrong while interacting with database
     */
    void deleteAllById(List<Long> ids) throws SQLException;

    /**
     * mark the records as archived in db
     * @param t - object to be archived
     * @throws SQLException - when something went wrong while interacting with database
     */
    void markAsArchived(T t) throws SQLException;
}
