package datastorage;

import model.Nurse;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Extension of {@link DAOimp} for {@link Nurse}
 */
public class NurseDAO extends DAOimp<Nurse>{

    private static final String ID_COLUMN_NAME = "NURSEID";
    private static final String FIRSTNAME_COLUMN_NAME = "FIRSTNAME";
    private static final String SURNAME_COLUMN_NAME = "SURNAME";
    private static final String PHONENUMBER_COLUMN_NAME = "PHONENUMBER";
    private static final String EXIT_DATE_COLUMN_NAME = "EXIT_DATE";
    private static final String ARCHIVE_COLUMN_NAME = "ARCHIVE";
    private static final String ENTER_DATE_COLUMN_NAME = "ENTER_DATE";

    public NurseDAO(Connection conn) {
        super(conn);
    }

    /**
     * generates a <code>INSERT INTO</code>-Statement for a given nurse
     * @param nurse for which a specific INSERT INTO is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(Nurse nurse) {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT INTO NURSE (")
                .append(FIRSTNAME_COLUMN_NAME + ", ")
                .append(SURNAME_COLUMN_NAME + ", ")
                .append(PHONENUMBER_COLUMN_NAME + ", ")
                .append(ENTER_DATE_COLUMN_NAME + ", ")
                .append(EXIT_DATE_COLUMN_NAME + ", ")
                .append(ARCHIVE_COLUMN_NAME).append(") ")
                .append("VALUES (")
                .append("'").append(nurse.getFirstName()).append("', ")
                .append("'").append(nurse.getSurname()).append("', ")
                .append("'").append(nurse.getPhoneNumber()).append("', ")
                .append("'").append(nurse.getEnterDate()).append("', ")
                .append("'").append(nurse.getExitDate()).append("', ")
                .append("'").append(nurse.getArchive()).append("')");
        return builder.toString().replace("'null'", "null");
    }

    /**
     * generates a <code>select</code>-Statement to get a nurse record from db with given key
     * @param key for which a specific SELECT is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM nurse WHERE nurseID = %d", key);
    }


    @Override
    protected Nurse getInstanceFromResultSet(ResultSet result) throws SQLException {
        Nurse nurse = null;
        LocalDate date = DateConverter.convertStringToLocalDate(result.getString(ENTER_DATE_COLUMN_NAME));
        nurse = new Nurse(result.getInt(ID_COLUMN_NAME),
                result.getString(FIRSTNAME_COLUMN_NAME),
                date,
                result.getString(SURNAME_COLUMN_NAME),
                result.getString(PHONENUMBER_COLUMN_NAME));
        nurse.setArchive(result.getBoolean(ARCHIVE_COLUMN_NAME));
        nurse.setExitDate(DateConverter.convertStringToLocalDate(result.getString(EXIT_DATE_COLUMN_NAME)));
        return nurse;
    }

    /**
     * generates a <code>SELECT</code>-Statement for all nurses.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM nurse";
    }

    /**
     * maps a <code>ResultSet</code> to a <code>Nurse-List</code>
     * @param result ResultSet with a multiple rows. Data will be mapped to patient-object.
     * @return ArrayList with patients from the resultSet.
     */
    @Override
    protected ArrayList<Nurse> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Nurse> list = new ArrayList<>();
        Nurse n = null;
        while(result.next()) {
            n = getInstanceFromResultSet(result);
            list.add(n);
        }
        return list;
    }

    /**
     * generates a <code>UPDATE</code>-Statement for a given patient
     * @param nurse for which a specific update is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(Nurse nurse) {
        StringBuilder builder = new StringBuilder("UPDATE NURSE SET ");
        builder.append(FIRSTNAME_COLUMN_NAME).append(" = '").append(nurse.getFirstName()).append("', ")
                .append(SURNAME_COLUMN_NAME).append(" = '").append(nurse.getSurname()).append("', ")
                .append(PHONENUMBER_COLUMN_NAME).append(" = '").append(nurse.getPhoneNumber()).append("', ")
                .append(ENTER_DATE_COLUMN_NAME).append(" = '").append(nurse.getEnterDate()).append("', ")
                .append(EXIT_DATE_COLUMN_NAME).append(" = '").append(nurse.getExitDate()).append("', ")
                .append(ARCHIVE_COLUMN_NAME).append(" = '").append(nurse.getArchive()).append("' ")
                .append("WHERE NURSEID = ")
                .append(nurse.getNurseID());
        return builder.toString();
    }

    /**
     * generates a <code>delete</code>-Statement to delete a nurse with a given key
     * @param key for which a specific DELETE is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("DELETE FROM nurse WHERE nurseID=%d", key);
    }


    @Override
    protected String getMarkArchivedStatement(Nurse nurse) {
        return "UPDATE NURSE SET " + ARCHIVE_COLUMN_NAME + " = 'true' WHERE NURSEID = " + nurse.getNurseID();
    }
}
