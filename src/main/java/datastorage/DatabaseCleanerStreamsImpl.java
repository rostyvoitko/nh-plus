package datastorage;

import model.Nurse;
import model.Patient;
import model.Treatment;
import utils.DateConverter;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Class that uses {@link DAOFactory} to delete old patient related data regrading DSGVO rules.
 */
public class DatabaseCleanerStreamsImpl implements DatabaseCleaner{

    private final PatientDAO patientDAO;
    private final NurseDAO nurseDAO;
    private final TreatmentDAO treatmentDAO;
    private final DatabaseCleanerStreamsUtil util;

    public DatabaseCleanerStreamsImpl() {
        DAOFactory factory = DAOFactory.getDAOFactory();
        this.nurseDAO = factory.createNurseDAO();
        this.patientDAO = factory.createPatientDAO();
        this.treatmentDAO = factory.createTreatmentDAO();
        this.util = new DatabaseCleanerStreamsUtil();
    }

    @Override
    public List<Treatment> deleteOldTreatments() throws SQLException {
        List<Treatment> treatmentsToDelete = util.getTreatmentsToDelete(treatmentDAO.readAll());
        treatmentDAO.deleteAllById(treatmentsToDelete.stream().map(Treatment::getTid).collect(Collectors.toList()));
        return treatmentsToDelete;
    }

    @Override
    public List<Patient> deleteOldArchivedPatientsData() throws SQLException {
        List<Patient> patientsToDelete = util.getPersonsToDelete(util.getArchivedPersons(patientDAO.readAll()));
        patientDAO.deleteAllById(patientsToDelete.stream().map(Patient::getPid).collect(Collectors.toList()));
        for(Patient p : patientsToDelete) {
            treatmentDAO.deleteByPid(p.getPid());
        }
        return patientsToDelete;
    }

    @Override
    public List<Nurse> deleteArchivedNurses() throws SQLException {
        List<Nurse> nursesToDelete = util.getPersonsToDelete(util.getArchivedPersons(nurseDAO.readAll()));
        nurseDAO.deleteAllById(nursesToDelete.stream().map(Nurse::getNurseID).collect(Collectors.toList()));
        return nursesToDelete;
    }

    @Override
    public List<Patient> deleteOldPatients() throws SQLException {
        List<Patient> deletedPatienten = new ArrayList<>();

        for(Patient p : patientDAO.readAll()) {
            List<Treatment> patientsTreatments = treatmentDAO.readTreatmentsByPid(p.getPid());
            if(!patientsTreatments.isEmpty()) {
                Optional<Treatment> latest = util.findLatestTreatment(patientsTreatments);
                if(latest.isPresent() && util.hasTenYearsFromNow(LocalDate.parse(latest.get().getDate(), DateConverter.getFormatter()))) {
                    patientDAO.deleteById(p.getPid());
                    deletedPatienten.add(p);
                }
            } else {
                if(util.hasTenYearsFromNow(p.getEnterDate())) {
                    patientDAO.deleteById(p.getPid());
                    deletedPatienten.add(p);
                }
            }
        }
        return deletedPatienten;
    }
}
