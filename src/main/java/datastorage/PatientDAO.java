package datastorage;

import model.Patient;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific patient-SQL-queries.
 */
public class PatientDAO extends DAOimp<Patient> {

    private static final String ID_COLUMN = "PID";
    private static final String FIRST_NAME_COLUMN = "FIRSTNAME";
    private static final String SURNAME_COLUMN = "SURNAME";
    private static final String DATE_OF_BIRTH_COLUMN = "DATEOFBIRTH";
    private static final String CARE_LEVEL_COLUMN = "CARELEVEL";
    private static final String ROOM_NUMBER_COLUMN = "ROOMNUMBER";
    private static final String ENTER_DATE_COLUMN = "ENTER_DATE";
    private static final String EXIT_DATE_COLUMN = "EXIT_DATE";
    private static final String ARCHIVE_COLUMN = "ARCHIVE";

    /**
     * constructs Onbject. Calls the Constructor from <code>DAOImp</code> to store the connection.
     * @param conn
     */
    public PatientDAO(Connection conn) {
        super(conn);
    }

    /**
     * generates a <code>INSERT INTO</code>-Statement for a given patient
     * @param patient for which a specific INSERT INTO is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(Patient patient) {
        StringBuilder builer = new StringBuilder("INSERT INTO PATIENT (");
        builer.append(FIRST_NAME_COLUMN).append(", ")
                .append(SURNAME_COLUMN).append(", ")
                .append(DATE_OF_BIRTH_COLUMN).append(", ")
                .append(CARE_LEVEL_COLUMN).append(", ")
                .append(ROOM_NUMBER_COLUMN).append(", ")
                .append(ENTER_DATE_COLUMN).append(", ")
                .append(EXIT_DATE_COLUMN).append(", ")
                .append(ARCHIVE_COLUMN).append(") ")
                .append("VALUES (")
                .append("'").append(patient.getFirstName()).append("', ")
                .append("'").append(patient.getSurname()).append("', ")
                .append("'").append(patient.getDateOfBirth()).append("', ")
                .append("'").append(patient.getCareLevel()).append("', ")
                .append("'").append(patient.getRoomnumber()).append("', ")
                .append("'").append(patient.getEnterDate()).append("', ")
                .append("'").append(patient.getExitDate()).append("', ")
                .append("'").append(patient.getArchive()).append("') ");
        return builer.toString().replace("'null'", "null");
    }

    /**
     * generates a <code>select</code>-Statement for a given key
     * @param key for which a specific SELECTis to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM patient WHERE pid = %d", key);
    }

    /**
     * maps a <code>ResultSet</code> to a <code>Patient</code>
     * @param result ResultSet with a single row. Columns will be mapped to a patient-object.
     * @return patient with the data from the resultSet.
     */
    @Override
    protected Patient getInstanceFromResultSet(ResultSet result) throws SQLException {
        Patient p = null;
        p = new Patient(result.getInt(ID_COLUMN),
                result.getString(FIRST_NAME_COLUMN),
                DateConverter.convertStringToLocalDate(result.getString(ENTER_DATE_COLUMN)),
                result.getString(SURNAME_COLUMN),
                DateConverter.convertStringToLocalDate(result.getString(DATE_OF_BIRTH_COLUMN)),
                result.getString(CARE_LEVEL_COLUMN),
                result.getString(ROOM_NUMBER_COLUMN));
        p.setExitDate(DateConverter.convertStringToLocalDate(result.getString(EXIT_DATE_COLUMN)));
        p.setArchive(result.getBoolean(ARCHIVE_COLUMN));
        return p;
    }

    /**
     * generates a <code>SELECT</code>-Statement for all patients.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM patient";
    }

    /**
     * maps a <code>ResultSet</code> to a <code>Patient-List</code>
     * @param result ResultSet with a multiple rows. Data will be mapped to patient-object.
     * @return ArrayList with patients from the resultSet.
     */
    @Override
    protected ArrayList<Patient> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Patient> list = new ArrayList<Patient>();
        Patient p = null;
        while (result.next()) {
            p = getInstanceFromResultSet(result);
            list.add(p);
        }
        return list;
    }

    /**
     * generates a <code>UPDATE</code>-Statement for a given patient
     * @param patient for which a specific update is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(Patient patient) {
        return String.format("UPDATE patient SET firstname = '%s', surname = '%s', dateOfBirth = '%s', carelevel = '%s', " +
                "roomnumber = '%s' WHERE pid = %d", patient.getFirstName(), patient.getSurname(), patient.getDateOfBirth(),
                patient.getCareLevel(), patient.getRoomnumber(), patient.getPid());
    }

    /**
     * generates a <code>delete</code>-Statement for a given key
     * @param key for which a specific DELETE is to be created
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM patient WHERE pid=%d", key);
    }

    @Override
    protected String getMarkArchivedStatement(Patient patient) {
        return "UPDATE PATIENT SET " + ARCHIVE_COLUMN + " = true WHERE PID = " + patient.getPid();
    }
}
