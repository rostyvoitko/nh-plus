package datastorage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class that contains implementation of CRUD for a given Type and declares
 * abstract methods for inheritors
 * @param <T>
 */
public abstract class DAOimp<T> implements DAO<T>{
    protected Connection conn;

    protected DAOimp(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void create(T t) throws SQLException {
        try (Statement st = conn.createStatement()) {
            st.executeUpdate(getCreateStatementString(t));
        }
    }

    @Override
    public T read(long key) throws SQLException {
        T object = null;
        ResultSet result;
        try (Statement st = conn.createStatement()) {
            result = st.executeQuery(getReadByIDStatementString(key));
        }
        if (result.next()) {
            object = getInstanceFromResultSet(result);
        }
        return object;
    }

    @Override
    public List<T> readAll() throws SQLException {
        ArrayList<T> list = new ArrayList<>();
        ResultSet result;
        try (Statement st = conn.createStatement()) {
            result = st.executeQuery(getReadAllStatementString());
        }
        list = getListFromResultSet(result);
        return list;
    }

    @Override
    public void update(T t) throws SQLException {
        try (Statement st = conn.createStatement()) {
            st.executeUpdate(getUpdateStatementString(t));
        }
    }

    @Override
    public void deleteById(long key) throws SQLException {
        try (Statement st = conn.createStatement()) {
            st.executeUpdate(getDeleteStatementString(key));
        }
    }

    @Override
    public void deleteAllById(List<Long> ids) throws SQLException {
        for(long id: ids) {
            deleteById(id);
        }
    }

    @Override
    public void markAsArchived(T t) throws SQLException {
        try (Statement statement = conn.createStatement()) {
            statement.executeUpdate(getMarkArchivedStatement(t));
        }
    }

    /**
     * @param t - Object of given type
     * @return - SQL String to mark object archived in db
     */
    protected abstract String getMarkArchivedStatement(T t);

    /**
     * @param t - Object of given class type parameter
     * @return - SQL String to create a t in db
     */
    protected abstract String getCreateStatementString(T t);

    /**
     *
     * @param key - id of some record
     * @return - SQL String to Delete this record by id
     */
    protected abstract String getReadByIDStatementString(long key);

    /**
     * turns {@link ResultSet} to object of class type parameter
     * @param set - {@link ResultSet}
     * @return - object from result set
     * @throws SQLException - happens when problem with db interaction
     */
    protected abstract T getInstanceFromResultSet(ResultSet set) throws SQLException;

    /**
     *
     * @return SQL String to read all records from database
     */
    protected abstract String getReadAllStatementString();

    /**
     *
     * @param set - {@link ResultSet}
     * @return - list of object from given result set
     * @throws SQLException - happens when problem with db interaction
     */
    protected abstract ArrayList<T> getListFromResultSet(ResultSet set) throws SQLException;

    /**
     *
     * @param t - object to update in db
     * @return - SQL String to update given object
     */
    protected abstract String getUpdateStatementString(T t);

    /**
     *
     * @param key - id of object to delete
     * @return - delete a record with given key from db
     */
    protected abstract String getDeleteStatementString(long key);

    /**
     * @return singleton database connection
     */
    public Connection getConn() {
        return conn;
    }
}
