package datastorage;

import model.Nurse;
import model.Patient;
import model.Treatment;

import java.sql.SQLException;
import java.util.List;

/**
 * Interface to clean the database regarding DSVGO rules after {@link DataStorageConstants#YEARS_TO_STORE_DATA} years
 */
public interface DatabaseCleaner {

    /**
     * deletes old patients records from db.
     * patient has latest treatment 10 years ago? delete patient.
     * @return - list of deleted patients to show in a summary after deletion
     */
    List<Patient> deleteOldPatients() throws SQLException;

    /**
     * delete old treatment records from db.
     * treatment is older than 10 years? delete treatment.
     * @return - list of deleted treatments
     */
    List<Treatment> deleteOldTreatments() throws SQLException;

    /**
     * delete records of patients that are archived for 10 years from db.
     * patient is archived more then 10 years ago ? delete.
     * @return list of deleted patients to show in summary after deletion
     */
    List<Patient> deleteOldArchivedPatientsData() throws SQLException;

    /**
     * delete nurse records that are 10 years already archived from db.
     * Nurse is archived more than 10 years ago? delete
     * @return list of deleted nurses to show in summary after deletion
     */
    List<Nurse> deleteArchivedNurses() throws SQLException;
}
