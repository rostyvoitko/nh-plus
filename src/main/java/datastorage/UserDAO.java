package datastorage;

import model.User;
import utils.PasswordHasher;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Extender of {@link DAOimp} for {@link User} model
 */
public class UserDAO extends DAOimp<User>{

    public UserDAO(Connection conn) {
        super(conn);
    }

    public User getUserByUsername(String username) throws SQLException {
        try (Statement statement = getConn().createStatement()) {
            ResultSet resultSet = statement.executeQuery(getReadByUsernameString(username));
            return getInstanceFromResultSet(resultSet);
        }
    }

    @Override
    protected String getMarkArchivedStatement(User user) {
        return "";
    }

    /**
     *
     * @param user user object from database
     * @return string - sql query
     */
    protected String getPasswordFromDatabaseByUsername(User user) {
        return String.format("SELECT Passwort FROM USER WHERE Benutzername = %s",
                        user.getUserName());
    }

    /**
     *
     * @param user to be insetred into databas
     * @return a String as a SQL Query to insert a new User into the database
     */
    @Override
    protected String getCreateStatementString(User user) {
        String hashedPassword = PasswordHasher.doHashing(user.getPassword());
        return String.format("INSERT INTO USER (BENUTZERNAME, PASSWORT) VALUES ('%s', '%s')",
                user.getUserName(), hashedPassword);
    }

    public String getReadByUsernameString(String username) {
        StringBuilder stringBuilder = new StringBuilder("SELECT * FROM USER WHERE BENUTZERNAME = ");
        stringBuilder.append("'").append(username).append("'");
        return stringBuilder.toString();
    }

    /**
     *
     * @param key is the id from which the User is selected
     * @return a query as a String to search for a specific user by a certain id
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM USER WHERE ID = %d", key);
    }

    /**
     *
     * @param set the resultset
     * @return a userobject from a resultset
     * @throws SQLException when nothing is found
     */
    @Override
    protected User getInstanceFromResultSet(ResultSet set) throws SQLException {
        if (set.next()) {
            return new User(set.getString("BENUTZERNAME"), set.getString("PASSWORT"));
        }
        return null;
    }

    /**
     *
     * @return all users and passwords fro database
     */
    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM USER";
    }

    /**
     * @param set - result set
     * @return list of users from result set
     * @throws SQLException - when error while performing sql operations
     */
    @Override
    protected ArrayList<User> getListFromResultSet(ResultSet set) throws SQLException {
        ArrayList<User> resultSet = new ArrayList<>();
        while(set.next()) {
            getInstanceFromResultSet(set);
        }
        return resultSet;
    }

    /**
     *
     * @param user object to get updated
     * @return a query as a string to update a user in database
     */
    @Override
    protected String getUpdateStatementString(User user) {
        return null;
    }

    /**
     *
     * @param key is the id from the user to be deleted
     * @return a query as a String to delete a user by his id
     */
    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM USER WHERE id=%d", key);
    }

}
