package datastorage;

import model.Treatment;
import utils.DateConverter;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Extender of {@link DAOimp} for {@link Treatment}
 */
public class TreatmentDAO extends DAOimp<Treatment> {

    private static final String ID_COLUMN = "TID";
    private static final String PATIENT_ID_COLUMN = "PID";
    private static final String TREATMENT_DATE_COLUMN = "TREATMENT_DATE";
    private static final String BEGIN_COLUMN = "BEGIN";
    private static final String END_COLUMN = "END";
    private static final String DESCRIPTION_COLUMN = "DESCRIPTION";
    private static final String REMARKS_COLUMN = "REMARKS";
    private static final String ARCHIVE_COLUMN = "ARCHIVE";
    private static final String NURSE_ID_COLUMN = "NURSE_ID";
    private static final List<String> COLUMNS_LIST = Arrays.asList(
            PATIENT_ID_COLUMN, TREATMENT_DATE_COLUMN, BEGIN_COLUMN,
            END_COLUMN, DESCRIPTION_COLUMN, REMARKS_COLUMN, ARCHIVE_COLUMN, NURSE_ID_COLUMN);

    public TreatmentDAO(Connection conn) {
        super(conn);
    }

    @Override
    protected String getCreateStatementString(Treatment treatment) {
        List<Object> treatmentPropertiesList = Arrays.asList(
                treatment.getPid(), treatment.getDate(),
                treatment.getBegin(), treatment.getEnd(),
                treatment.getDescription(), treatment.getRemarks(),
                treatment.getArchive(), treatment.getNurseId());

        StringBuilder builder = new StringBuilder("INSERT INTO TREATMENT ");

        builder.append(COLUMNS_LIST.stream()
                        .collect(Collectors.joining(", ", "(", ")")))
                .append(" VALUES ").append(treatmentPropertiesList.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining("', '", "('", "')")));
        return builder.toString();
    }

    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM treatment WHERE tid = %d", key);
    }

    @Override
    protected Treatment getInstanceFromResultSet(ResultSet result) throws SQLException {
        LocalDate date = DateConverter.convertStringToLocalDate(result.getString(TREATMENT_DATE_COLUMN));
        LocalTime begin = DateConverter.convertStringToLocalTime(result.getString(BEGIN_COLUMN));
        LocalTime end = DateConverter.convertStringToLocalTime(result.getString(END_COLUMN));
        Treatment t = new Treatment(result.getLong(ID_COLUMN), result.getLong(PATIENT_ID_COLUMN),
                date, begin, end, result.getString(DESCRIPTION_COLUMN), result.getString(REMARKS_COLUMN));
        t.setNurseId(result.getString(NURSE_ID_COLUMN));
        t.setArchive(result.getBoolean(ARCHIVE_COLUMN));
        return t;
    }

    @Override
    protected String getReadAllStatementString() {
        return "SELECT * FROM treatment WHERE archive = false OR archive IS NULL";
    }

    @Override
    protected ArrayList<Treatment> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Treatment> list = new ArrayList<>();
        Treatment t = null;
        while (result.next()) {
            t = getInstanceFromResultSet(result);
            list.add(t);
        }
        return list;
    }

    @Override
    protected String getUpdateStatementString(Treatment treatment) {
        String sql = String.format("UPDATE treatment SET pid = %d, treatment_date ='%s', begin = '%s', end = '%s'," +
                "description = '%s', remarks = '%s', NURSE_ID = '%s' WHERE tid = %d", treatment.getPid(), treatment.getDate(),
                treatment.getBegin(), treatment.getEnd(), treatment.getDescription(), treatment.getRemarks(), treatment.getNurseId(),
                treatment.getTid());
        return sql.replace("'null'", "null");
    }

    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM treatment WHERE tid= %d", key);
    }

    public List<Treatment> readTreatmentsByPid(long pid) throws SQLException {
        List<Treatment> list;
        ResultSet result;
        try (Statement st = conn.createStatement()) {
            result = st.executeQuery(getReadAllTreatmentsOfOnePatientByPid(pid));
        }
        list = getListFromResultSet(result);
        return list;
    }

    private String getReadAllTreatmentsOfOnePatientByPid(long pid){
        return String.format("SELECT * FROM treatment WHERE pid = %d", pid);
    }

    /**
     * delete by id
     * @param key id to delete
     * @throws SQLException - error while db interaction
     */
    public void deleteByPid(long key) throws SQLException {
        try (Statement st = conn.createStatement()) {
            st.executeUpdate(String.format("Delete FROM treatment WHERE pid = %d", key));
        }
    }

    @Override
    protected String getMarkArchivedStatement(Treatment treatment) {
        return "UPDATE TREATMENT SET " + ARCHIVE_COLUMN + " = 'true' WHERE TID = " + treatment.getTid();
    }
}