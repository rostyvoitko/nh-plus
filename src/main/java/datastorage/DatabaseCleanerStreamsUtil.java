package datastorage;

import model.Person;
import model.Treatment;
import utils.DateConverter;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class for {@link DatabaseCleaner}
 */
public class DatabaseCleanerStreamsUtil {

    /**
     * Constructor for {@link DatabaseCleanerStreamsUtil}
     */
    public DatabaseCleanerStreamsUtil() {
        // no args constructor
    }

    /**
     * get persons that have archive field set to true
     * @param persons - list of persons
     * @return opened stream of archived persons
     * @param <T> - type of person
     */
    public <T extends Person> Stream<T> getArchivedPersons(List<T> persons) {
        return persons.stream()
                .filter(Person::getArchive);
    }

    /**
     * filter stream of persons by exit date > 10 years from now
     * @param persons - stream of persons
     * @return - list of persons that have exit date > 10 years from now
     * @param <T> - type of person
     */
    public <T extends Person> List<T> getPersonsToDelete(Stream<T> persons) {
        return persons
                .filter(n -> hasTenYearsFromNow(n.getExitDate()))
                .collect(Collectors.toList());
    }

    /**
     * filter list of treatments by date > 10 years from now
     * @param treatments - list of treatments to filter
     * @return list of treatments that have date > 10 years from now
     */
    public List<Treatment> getTreatmentsToDelete(List<Treatment> treatments) {
        return treatments.stream().filter(p -> {
            LocalDate date = LocalDate.parse(p.getDate(), DateConverter.getFormatter());
            return hasTenYearsFromNow(date);
        }).collect(Collectors.toList());
    }

    /**
     * find the last treatment in list of treatments
     * @param treatments - list of treatments
     * @return - optional of latest treatment
     */
    public Optional<Treatment> findLatestTreatment(List<Treatment> treatments) {
        Treatment latest = null;
        for(Treatment t : treatments) {
            if(isLatest(latest, t)) {
                latest = t;
            }
        }
        return Optional.ofNullable(latest);
    }

    /**
     * compare two treatments by date
     * returns true is second argument is after first argument
     * @param latest - latest treatment
     * @param toCompare - treatment to compare
     * @return - true if second argument is after first argument
     */
    public boolean isLatest(Treatment latest, Treatment toCompare) {
        if(latest == null) {
            return true;
        }
        LocalDate toCompareDate = LocalDate.parse(toCompare.getDate());
        LocalDate latestDate = LocalDate.parse(latest.getDate());
        return toCompareDate.isAfter(latestDate);
    }

    /**
     * check if date is more than 10 years from now
     * @param date - date to check
     * @return - true if date is more than 10 years from now
     */
    public boolean hasTenYearsFromNow(LocalDate date) {
        if(date == null) {
            return false;
        }
        LocalDate now = LocalDate.now();
        LocalDate dateToCompare = LocalDate.from(date);
        LocalDate tenYearsFromNow = now.minusYears(DataStorageConstants.YEARS_TO_STORE_DATA);
        return dateToCompare.isBefore(tenYearsFromNow);
    }

}
