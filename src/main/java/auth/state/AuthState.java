package auth.state;

import auth.AuthenticationContext;

/**
 * Interface for different Authentication States
 */
public interface AuthState {

    /**
     * authenticate the user
     */
    void authenticate();

    /**
     * unauthenticate the user
     */
    void unauthenticate();

    /**
     * sets a context to perform actions on
     * @param authenticationContext - {@link AuthenticationContext}
     */
    void setAuthenticationContext(AuthenticationContext authenticationContext);
}
