package auth.state;

import auth.AuthenticationContext;

/**
 * State that indicates that user was not authenticated and authorized
 */
public class UnauthenticatedAuthState implements AuthState {

    AuthenticationContext authenticationContext;

    @Override
    public void authenticate() {
        AuthenticatedAuthState authenticatedAuthState = new AuthenticatedAuthState();
        authenticatedAuthState.setAuthenticationContext(authenticationContext);
        authenticationContext.changeState(authenticatedAuthState);
    }

    @Override
    public void unauthenticate() {
        authenticationContext.changeState(this);
    }

    @Override
    public void setAuthenticationContext(AuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }
}
