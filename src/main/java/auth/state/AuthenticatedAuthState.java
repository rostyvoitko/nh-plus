package auth.state;

import auth.AuthenticationContext;

/**
 * State that indicates that user was successfully authenticated and authorized to see the content
 */
public class AuthenticatedAuthState implements AuthState {

    AuthenticationContext authenticationContext;

    @Override
    public void authenticate() {
        // can not authenticate authenticated state
    }

    @Override
    public void unauthenticate() {
        UnauthenticatedAuthState unauthenticatedAuthState = new UnauthenticatedAuthState();
        unauthenticatedAuthState.setAuthenticationContext(authenticationContext);
        authenticationContext.changeState(unauthenticatedAuthState);
    }

    @Override
    public void setAuthenticationContext(AuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }
}
