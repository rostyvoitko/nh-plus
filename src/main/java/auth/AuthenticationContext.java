package auth;

import auth.state.AuthState;
import auth.state.AuthenticatedAuthState;
import auth.state.UnauthenticatedAuthState;

/**
 * Class to track and control the overall app's authentication state.
 * Class is a Singleton.
 */
public class AuthenticationContext {

    private static AuthenticationContext context = null;
    private AuthState authState;

    /**
     * private constructor to hide a public one
     */
    private AuthenticationContext() {
    }

    /**
     * get the singleton instance of AuthenticationContext
     * @return - {@link AuthenticationContext}
     */
    public static AuthenticationContext getContext() {
        if (context == null) {
            context = new AuthenticationContext();
            UnauthenticatedAuthState startingState = new UnauthenticatedAuthState();
            startingState.setAuthenticationContext(context);
            context.setAuthState(startingState);
        }
        return context;
    }

    /**
     * method to get the instance of active auth state
     * @return - {@link AuthState} - active auth state
     */
    public AuthState getAuthState() {
        return authState;
    }

    private void setAuthState(AuthState authState) {
        this.authState = authState;
    }

    /**
     * method to change the active Authentication Context's AuthState
     * @param state - {@link AuthState} new active state
     */
    public void changeState(AuthState state) {
        this.authState = state;
    }

    /**
     * Calls the {@link AuthState#authenticate()} method on current context's state
     */
    public static void authorize() {
        getContext().getAuthState().authenticate();
    }

    /**
     * Calls the {@link AuthState#unauthenticate()} method on current context's state
     */
    public static void logout() {
        getContext().getAuthState().unauthenticate();
    }

    /**
     * checks the auth state
     * @return - true if current state is authenticated, otherwise false
     */
    public static boolean isAuthorized() {
        return getContext().getAuthState() instanceof AuthenticatedAuthState;
    }
}
