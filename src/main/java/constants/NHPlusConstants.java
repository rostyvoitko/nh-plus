package constants;

/**
 * Constants for NHPlus app
 */
public class NHPlusConstants {
    private NHPlusConstants() {

    }

    public static final String CLEANED_MESSAGE = "Datenbank bereinigt!";
    public static final String ERROR_DB_MESSAGE = "Fehler beim Bereinigen der Datenbank";
}
