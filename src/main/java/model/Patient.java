package model;

import utils.DateConverter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Patients live in a NURSING home and are treated by nurses.
 * Extends {@link Person} and has additional fields that are specific for a patient
 */
public class Patient extends Person {
    private long pid;
    private LocalDate dateOfBirth;
    private String careLevel;
    private String roomnumber;
    private List<Treatment> allTreatments = new ArrayList<Treatment>();



    /**
     * constructs a patient from the given params.
     * @param pid - patient id
     * @param firstName - first name
     * @param surname - last name
     * @param dateOfBirth - date of birth
     * @param careLevel - level of care
     * @param roomnumber - room number
     */
    public Patient(long pid, String firstName, LocalDate enterDate, String surname, LocalDate dateOfBirth, String careLevel, String roomnumber) {
        super(firstName, surname, enterDate);
        this.pid = pid;
        this.dateOfBirth = dateOfBirth;
        this.careLevel = careLevel;
        this.roomnumber = roomnumber;
    }

    public Patient(String firstName, String surname, LocalDate enterDate, long pid, LocalDate dateOfBirth, String careLevel, String roomnumber) {
        super(firstName, surname, enterDate);
        this.pid = pid;
        this.dateOfBirth = dateOfBirth;
        this.careLevel = careLevel;
        this.roomnumber = roomnumber;
    }

    /**
     * constructor to construct a patient without id
     * @param firstname - firstname
     * @param surname - last name
     * @param enterDate - date of coming
     * @param dateOfBirth - date of birth
     * @param carelevel - level of care
     * @param room - room number
     */
    public Patient(String firstname, String surname, LocalDate enterDate, LocalDate dateOfBirth, String carelevel, String room) {
        super(firstname, surname, enterDate);
        this.dateOfBirth = dateOfBirth;
        this.careLevel = carelevel;
        this.roomnumber = room;
    }

    /**
     *
     * @return patient id
     */
    public long getPid() {
        return pid;
    }

    /**
     *
     * @return date of birth as a string
     */
    public String getDateOfBirth() {
        return dateOfBirth.toString();
    }

    /**
     * convert given param to a localDate and store as new <code>birthOfDate</code>
     * @param dateOfBirth as string in the following format: YYYY-MM-DD
     */
    public void setDateOfBirth(String dateOfBirth) {
        LocalDate birthday = DateConverter.convertStringToLocalDate(dateOfBirth);
        this.dateOfBirth = birthday;
    }

    /**
     *
     * @return careLevel
     */
    public String getCareLevel() {
        return careLevel;
    }

    /**
     *
     * @param careLevel new care level
     */
    public void setCareLevel(String careLevel) {
        this.careLevel = careLevel;
    }

    /**
     *
     * @return roomNumber as string
     */
    public String getRoomnumber() {
        return roomnumber;
    }

    /**
     *
     * @param roomnumber
     */
    public void setRoomnumber(String roomnumber) {
        this.roomnumber = roomnumber;
    }

    /**
     *

    /**
     * adds a treatment to the treatment-list, if it does not already contain it.
     * @param m Treatment
     * @return true if the treatment was not already part of the list. otherwise false
     */
    public boolean add(Treatment m) {
        if (!this.allTreatments.contains(m)) {
            this.allTreatments.add(m);
            return true;
        }
        return false;
    }

    /**
     *
     * @return string-representation of the patient
     */
    @Override
    public String toString() {
        return "Patient{" +
                "pid=" + pid +
                ", dateOfBirth=" + dateOfBirth +
                ", careLevel='" + careLevel + '\'' +
                ", roomnumber='" + roomnumber + '\'' +
                ", allTreatments=" + allTreatments +
                '}';
    }
}