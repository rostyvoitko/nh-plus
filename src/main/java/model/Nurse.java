package model;

import java.time.LocalDate;

/**
 * Nurse data class for a NHPlus application.
 * It extends {@link Person} class and have additional nurseID and phoneNumber fields
 */
public class Nurse extends Person{

    private long nurseID;
    private final String phoneNumber;

    public Nurse(long nurseID, String firstName, LocalDate enterDate, String surname, String phoneNumber) {
        super(firstName, surname, enterDate);
        this.nurseID = nurseID;
        this.phoneNumber = phoneNumber;
    }

    public Nurse(String firstName, String surname, LocalDate enterDate, String phoneNumber) {
        super(firstName, surname, enterDate);
        this.phoneNumber = phoneNumber;
    }

    public Nurse(String firstName, String surname, LocalDate enterDate, LocalDate endDate, String phoneNumber) {
        super(firstName, surname, enterDate, endDate);
        this.phoneNumber = phoneNumber;
    }

    public long getNurseID() {
        return nurseID;
    }

    public void setNurseID(long nurseID) {
        this.nurseID = nurseID;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return "Nurse{" +
                "nurseID=" + nurseID +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
