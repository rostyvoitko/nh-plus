package model;

import java.time.LocalDate;

/**
 * An abstract class for a persons related to Pflegeheim.
 */
public abstract class Person {
    private String firstName;
    private String surname;
    private LocalDate enterDate;
    private LocalDate exitDate;
    private Boolean archive;

    /**
     * Constructor for a Person instances
     * @param firstName - person's first name
     * @param surname - person's last name
     * @param enterDate - date of entrance
     */
    protected Person(String firstName, String surname, LocalDate enterDate) {
        this.firstName = firstName;
        this.surname = surname;
        this.enterDate = enterDate;
        this.exitDate = null; // person's exit date can be unknown when adding
        this.archive = false; // person record is not in archive by default
    }

    /**
     * OVERLOADED Constructor for a Person. MADE FOR A TEST REASONS
     * @param firstName - person's first name
     * @param surname - person's last name
     * @param enterDate - date of entrance
     * @param exitDate - date of exit
     */
    protected Person(String firstName, String surname, LocalDate enterDate, LocalDate exitDate) {
        this.firstName = firstName;
        this.surname = surname;
        this.enterDate = enterDate;
        this.exitDate = exitDate;
        this.archive = false; // person record is not in archive by default
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getEnterDate() {
        return enterDate;
    }

    public void setEnterDate(LocalDate enterDate) {
        this.enterDate = enterDate;
    }

    public LocalDate getExitDate() {
        return exitDate;
    }

    public void setExitDate(LocalDate exitDate) {
        this.exitDate = exitDate;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}
